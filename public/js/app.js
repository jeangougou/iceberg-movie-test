(function() {

	var app = angular.module('app', ['ngResource']);
	
app.filter('offset', function() {
  return function(input, start) {		
    start = parseInt(start, 10);
    return input.slice(start);
  };
});
	
	app.controller('MainCtrl', ['$scope', '$filter', 'movieDs', function($scope, $filter, movieDs) {
		
		$scope.dataSource = movieDs.getDefault();
		$scope.currentPage = 0;
		$scope.pageSize = 20;
		$scope.total = $scope.dataSource.length;
		$scope.$watch('dataSource.movies.length', function(val){
			$scope.elementsFound = val;
		});
		// update error message
		$scope.$watch('searchbar', function(val){
			$scope.errorMessage = (undefined !== val && val.length < 3) ? 'Enter at least three characters to begin search' : '';
		});
		
		$scope.search = function() {
			// early exit, min value not respected
			if($scope.searchbar === undefined || $scope.searchbar.length < 3)
			{
				$scope.dataSource = movieDs.getDefault();
				return;
			}
			
			// update datasource
			$scope.dataSource = movieDs.getSearch({q:$scope.searchbar});
		};
		
		
		//pagination
		$scope.prevPage = function() {
			if ($scope.currentPage > 0) {
				$scope.currentPage--;
			}
		};

		$scope.prevPageDisabled = function() {
			return $scope.currentPage === 0 ? "disabled" : "";
		};

		$scope.pageCount = function() {
			
			return Math.ceil($scope.elementsFound / $scope.pageSize)-1;
		};

		$scope.nextPage = function() {
			if ($scope.currentPage < $scope.pageCount()) {
				$scope.currentPage++;
			}
		};

		$scope.nextPageDisabled = function() {
			return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
		};
		
	}]);

	// movie api data source
	app.factory('movieDs', function($resource) {
		var ds = $resource('/api/movies/',  // :q/:ps/:pn
											 { }, 
											 { 
			getDefault: {
				method: "GET",
				url: ('/api/movies'),
			},
			getSearch: {
				method: "GET",
				params: {
					q: '@q',
				},
				url: ('/api/movies/:q'),
			},            
			getPaged: {
				method: "GET",
				params: {
					ps: '@ps',
					pn: '@pn',
				},
				url: ('/api/movies/:ps/:pn'),
			}
		});	
		return ds;
	});
})();
