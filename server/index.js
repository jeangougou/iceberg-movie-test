var express = require('express');
var app = express();
var fs = require('fs');
var path = require('path');
var bodyParser = require('body-parser');
var port = process.env.PORT || 8000;

function sortByProperty(property) {
	'use strict';
	return function (a, b) {
		var sortStatus = 0;
		if (a[property] < b[property]) {
			sortStatus = -1;
		} else if (a[property] > b[property]) {
			sortStatus = 1;
		}
		return sortStatus;
	};
}


// only for the purpose of the excercise makes sense to preload all data.json
var dataModel = JSON.parse(fs.readFileSync('./data.json'));
dataModel.movies = dataModel.movies.sort(sortByProperty('title'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))

app.use(express.static(__dirname + '/../public'));

// all elements available
app.get('/api/movies', function(req,res){
	res.status(200).json(
		{
			movies: dataModel.movies,
			message: 'Matched ' + dataModel.movies.length + ' of ' + dataModel.movies.length +' movies total',
			total : dataModel.movies.length,
			found: dataModel.movies.length
		}
		);
});

// searchable endpoint
app.get('/api/movies/:q?', function(req,res){
	res.setHeader('Content-Type', 'application/json');
	if(!!req.params.q)
	{
		var toMatch = req.params.q.toLowerCase();
		console.log('searching for: ' + toMatch);
		var foundElements = dataModel.movies.filter(function(item){
			return item.title.toLowerCase().indexOf(toMatch) != -1;
		});
		res.send(
			{
				movies: foundElements,
				message: 'Matched ' + foundElements.length + ' of ' + dataModel.movies.length +' movies total',
				total : dataModel.movies.length,
				found: foundElements.length
			}); 
	}
	next(); // skips to next matching endpoint
});

// optional pagination parameters, todo add server side pagination
app.get('/api/movies/:ps?/:pn?', function(req, res){
	res.setHeader('Content-Type', 'application/json');

	//******** pagination
	var pageSize = 20;
	var page = 0;

	if(req.params.ps){
		pageSize = req.params.ps;
	}

	if(req.params.pn){
		page = req.params.pn - 1;
	} 
	var skip = pageSize * page;
	var foundElements = dataModel.movies.slice(skip, skip+pageSize);

	//******** result composition
	// early exit
	if(0 == foundElements.length){
		res.send({movies:[], message:'No matching items'});
		return;
	}
	res.send(
		{
			movies: foundElements,
			message: 'Matched ' + foundElements.length + ' of ' + dataModel.movies.length +' movies total',
			total : dataModel.movies.length,
			found: foundElements.length
		});
});

app.listen(port, function(){
	console.log('App started on port 8000!');
});

/*

 - Backend
	- The server should serve server/data.json when a request is made to /api/movies
	- Clients should be able to request a subset of data
		- Allow limiting the number of items returned
		- Allow pagination of data, e.g. get 10 items from the 4th 'page'

 - When there is at least one matching movie:
 	- The following text is displayed: 'Matched X of Y movies total'
	- Where X is the number of movies found and Y is the total number of movies
	- For each movie, both Title and Year are displayed

 - When the search phrase does not match any movies, the following message should be displayed: 'No matching items'

*/