beforeEach(module('app'));

	var dataStore;

	beforeEach(inject(function ($injector) {
		dataStore 	= $injector.get('movieDs');
	}));

	it('default call retrieves 160 movies', function() {
		dataStore.getDefault().then(function(response) {
				expect(response.movies.length).toBe(160);
		})

});